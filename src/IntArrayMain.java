public class IntArrayMain {
	public static void main(String args[]){
		IntArray ia=new IntArray(10);
		for(int i=0;i<7;i++){
		ia.insert(0,i);
		}
		System.out.println(ia.toString());
		System.out.println("データの個数："+ia.size());
		System.out.println("合計："+ia.total());
		System.out.println("最大値："+ia.max());
		System.out.println("最小値："+ia.min());
		System.out.println("平均："+ia.average());
		System.out.println("分散："+ia.variance());
	}
}
