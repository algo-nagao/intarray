/*
 * 配列（番兵）を用いた基本操作
 * 通常のプログラムでは、有効なデータで配列が埋め尽くされているとするものが多い（現実的ではない）
 */
public class IntArray {
	int data[];
	int size = 0;
	// コンストラクタ（要素数で初期化）
	IntArray(int n){
		this.data  = new int[n];
		for(int i=0; i<data.length;i++){
			data[i] = -1;
		}
		this.size = 0;
	}
	// コンストラクタ（配列で初期化）
	IntArray(int[] data){
		this.data = data;
		this.size = size();
	}
	/**データの挿入（データをずらす）
	 * @param index
	 * @param x
	 * @return
	 */	
	boolean insert(int index, int x){
		if(index>this.size)
			return false;
		for(int i=this.size;i>index;i--)
			data[i]=data[i-1];
		data[index]=x;
		this.size++;
		return true;
	}
	/**データの削除（データをずらす）
	 * @param x
	 * @return
	 */
	boolean delete(int index, int x){
		this.data=new int[index];
		if(data[index]==-1)
			return false;
		for(int i=data.length;i>index;i++)
			data[i]=data[i+1];
		return true;
	}

	int indexOf(int x){
		//データが含まれているかどうか
		this.data=new int[x];
		int index = -1;
		for (int i=0; i < this.data.length; i++){
			if(this.data[i]==x){
				return i;
			}
		}
		return index;
	}

	void disp(){
		for(int i = 0; i<data.length; i++)
			System.out.println(data[i]);
	}

	/**
	 * 配列の要素数を返す（番兵法）
	 * @return
	 */
	int size(){
		int cnt=0;
		for(int i=0;i<data.length;i++) {
			if(data[i]==-1) break;
			cnt ++;
		}
		return cnt;
	}
	/**配列に格納されたデータの最大値を求めるメソッドを作成（番兵）
	 * @return m
	 */
	int max(){
		int m=Integer.MIN_VALUE;
		for(int i=0;i<this.size;i++){
			if(data[i]>m){
				m=data[i];
			}
		}
		return m;
	}
	/**配列に格納されたデータの最小値を求めるメソッドを作成（番兵）
	 * @return m
	 */
	int min(){
		int mi = Integer.MAX_VALUE;
		// TODO ここに作成
		return mi;
	}
	/**配列に格納されたデータの合計を求めるメソッドを作成（番兵）
	 * @return m
	 */
	int total(){
		int t=0;
		// TODO ここに作成
		return t;
	}
	/**配列に格納されたデータの平均値を求めるメソッドを作成する（番兵）
	 * @return ave
	 */
	double average(){
		double ave=0;
		// TODO ここに作成
		return ave;
	}
	/**配列に格納されたデータの分散を求めるメソッドを作成する（番兵）
	 * @return　v
	 */
	double variance(){
		double v=0;
		double ave = average();
		
		for(int i=0;i<data.length;i++){
			double b=data[i]-ave;
			v=b*b;
		}
		return v;
	}
	public String toString(){
		//配列の表示
		String str = "";
		for (int i=0; i < this.data.length; i++){
			str += this.data[i] + " ";
		}
		return str;
	}
}
