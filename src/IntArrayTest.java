import junit.framework.TestCase;

public class IntArrayTest extends TestCase {

	int [] _d1 = {1,2,3,4,5,-1,6,7,8};
	IntArray d1 = new IntArray(_d1);

	int [] _d2 = {1,2,3,-1,4,5,6,7,8};
	IntArray d2 = new IntArray(_d2);

	int [] _d3 = {1,2,3,4,5,6,7,8,-1};
	IntArray d3 = new IntArray(_d3);
	
	public void testSize() {
		assertEquals(5,d1.size());
		assertEquals(3,d2.size());
		assertEquals(8,d3.size());
	}

	public void testMax() {
		assertEquals(5,d1.max());
		assertEquals(3,d2.max());
		assertEquals(8,d3.max());
	}

	public void testMin() {
		assertEquals(1,d1.min());
		assertEquals(1,d2.min());
		assertEquals(1,d3.min());
	}

	public void testTotal() {
		assertEquals(15,d1.total());
		assertEquals(6,d2.total());
		assertEquals(36,d3.total());
	}

	public void testAverage() {
		assertEquals(15.0/5,d1.average());
		assertEquals(6.0/3,d2.average());
		assertEquals(36.0/8,d3.average());
	}

}
